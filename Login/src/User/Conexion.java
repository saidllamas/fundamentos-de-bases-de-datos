package User;

import java.sql.*;

public class Conexion{
    Connection conexion = realizarConexion("jdbc:sqlserver://localhost:1433;"+"DatabaseName=TallerMecanico;","root","root");
    
    /**
     * Construimos y consultamos todas las tablas
     * **/
    public Conexion(){
        registrarJDBC("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        try {
            Statement stmt = conexion.createStatement();
            //consulta a todas las tablas
            ResultSet rs = stmt.executeQuery("SELECT * FROM Clientes");
            ResultSet rs2 = stmt.executeQuery("SELECT * FROM EncabSolicita");
            ResultSet rs3 = stmt.executeQuery("SELECT * FROM Garantia");
            ResultSet rs4 = stmt.executeQuery("SELECT * FROM Servicio");
            ResultSet rs5 = stmt.executeQuery("SELECT * FROM Solicita");
            ResultSet rs6 = stmt.executeQuery("SELECT * FROM Vehiculo");
            rs.close();
            stmt.close();
            System.out.println("Preparado.");
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    /*
     * * Obtenermos el driver
     */
    static public boolean registrarJDBC(String driver){
        try{
            Class.forName(driver);
            return true;
        }catch(ClassNotFoundException ex){
            return false;
        }
    }
    
    /*
     * Establecer conexion
     */
    static public Connection realizarConexion(String url,String user,String pass){
        try {
            Connection con = DriverManager.getConnection(url,user,pass);
            return con;
        } catch(SQLException e){
            System.out.print("Error: "+e.getMessage());
            return null;
        }
    }
    
    public void cerrarConexion(Connection con){
        try {
            con.close();
            System.out.println("Conexion cerrada");
        } catch (SQLException e) {
            System.out.println("Error: "+e.getMessage());
        }
    }
    
    /*     Clientes        */
    
    public void agregarCliente(String nombre, String domicilio, int telefono){
        try {
            Statement stmt = conexion.createStatement();
            int i =stmt.executeUpdate("INSERT INTO Clientes VALUES('"+nombre+"','"+domicilio+"',"+telefono+")");
            if(i > 0) System.out.println("Agregado correctamente.");
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    public void editarCliente(int idCliente, String nombre, String domicilio, int telefono){
    }
    
    public String[] consultarClientes(){
        String array[] = new String[getNumeroRegistros("Clientes")];
        int i = 0;
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM Clientes");
            //almacenar registros
            while(rs.next()){
                array[i++] = ""+rs.getString(2); //almacena el nombre
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
        return array;
   }
    
    public boolean eliminarCliente(int id){
        try {
            Statement stmt = conexion.createStatement();
            int i = stmt.executeUpdate("DELETE FROM Clientes WHERE idCliente ='"+id+"'");
            stmt.close();
            if(i > 0){
                System.out.println("Eliminado correctamente.");
                return true;
            }else{
                return false;
            }
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
            return true;
        }
    }
    
    /*		Vehiculos		*/
    
    public void agregarVehiculo(int idCliente, String marca, String modelo, String placas, String observaciones){
        try {
            Statement stmt = conexion.createStatement();
            int i =stmt.executeUpdate("INSERT INTO Vehiculo VALUES("+idCliente+",'"+marca+"','"+modelo+"','"+placas+"','"+observaciones+"')");
            if(i > 0) System.out.println("Agregado correctamente.");
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    public void editarVehiculo(int idVehiculo, int idCliente, String marca, String modelo, String placas, String observaciones){
    }
    
    public String[] consultarVehiculos(){
        String array[] = new String[getNumeroRegistros("Vehiculo")];
        int i = 0;
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT Placas FROM Vehiculo");
            //almacenar registros
            while(rs.next()){
                array[i++] = ""+rs.getString(1);
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
        return array;
    }
    
    public String[] consultarVehiculos(String nombreCliente){
        String array[] = new String[getNumeroRegistros("Vehiculo")];
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM Vehiculo, Clientes WHERE Vehiculo.idCliente=Clientes.idCliente AND Nombre='"+nombreCliente+"'");
            while(rs.next()){
                System.out.println(rs.getString(1)+", "+rs.getString(2)+", "+rs.getString(3)+", "+rs.getString(4)+", "+rs.getString(5)+", "+rs.getString(6));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
        return array;
    }
    
    public void eliminarVehiculo(String placas){
        try {
            Statement stmt = conexion.createStatement();
            int i = stmt.executeUpdate("DELETE FROM Vehiculo WHERE Placas ='"+placas+"'");
            if(i > 0) System.out.println("Eliminado correctamente. "+i+" filas afectadas.");
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    /*		Servicios		*/
    
    public void agregarServicio(String nombre, int costo){
        try {
            Statement stmt = conexion.createStatement();
            int i = stmt.executeUpdate("INSERT INTO Servicio VALUES('"+nombre+"',"+costo+")");
            if(i > 0) System.out.println("Agregado correctamente.");
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    public void editarServicio(){   
    }
    
    public String[] consultarServicios(){
        String array[] = new String[getNumeroRegistros("Servicio")];
        int i = 0;
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Servicio");
            //almacenar registros
            while(rs.next()){
                array[i++] = ""+rs.getString(2); //almacena el nombre
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }   
        return array;
    }
    
    public void consultarServicios(String nombre){
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Servicio WHERE nombreServicio='"+nombre+"'");
            while(rs.next()){
                System.out.println(rs.getString(1)+", "+rs.getString(2)+", "+rs.getString(3));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    public int getCosto(String name){
        int costo = 0;
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT Costo FROM Servicio WHERE nombreServicio='"+name+"'");
            while(rs.next()){
                costo = Integer.parseInt(rs.getString(1));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
        return costo;
    }
    
    public void eliminarServicio(String servicio){
        try {
            Statement stmt = conexion.createStatement();
            int i = stmt.executeUpdate("DELETE FROM Servicio WHERE nombreServicio ='"+servicio+"'");
            if(i > 0) System.out.println("Eliminado correctamente.");
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    /*		Solcitudes		*/
    
    public void agregarSolicitud(int idServicio, int idCliente, int idVehiculo, String fechaInicial, String fechaLimite, int subTotal, int total ){
        try {
            Statement stmt = conexion.createStatement();
            int i = stmt.executeUpdate("INSERT INTO Solicita VALUES("+idServicio+","+subTotal+")");
            if(i > 0) System.out.println("Agregado correctamente.");
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    public void consultarSolicitudes(){
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Solicita");
            while(rs.next()){
                System.out.println(rs.getString(1)+", "+rs.getString(2)+", "+rs.getString(3));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    public void consultarSolicitudes(int idServicio){
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM Solicita WHERE idServicio= "+idServicio+"");
            while(rs.next()){
                System.out.println(rs.getString(1)+", "+rs.getString(2)+", "+rs.getString(3));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    public void eliminarSolicitud(int idSolicita){
        try {
            Statement stmt = conexion.createStatement();
            int i = stmt.executeUpdate("DELETE FROM Solicita WHERE idSolicita="+idSolicita);
            if(i>0) System.out.println("Eliminado correctamente.");
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
    
    /*     AYUDA GENERAL */
    
    public int getNumeroRegistros(String table){
        int tuplas = 0;
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM "+table);
            while(rs.next()){
                tuplas++;
            }
        }catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
        return tuplas;
    }
    
    public int getID(String table, String name){
        int ID = 0;
        String condicion = "";
        switch(table){
            case "Clientes":
                condicion = "Nombre";
                break;
            case "Servicio":
                condicion = "nombreServicio";
                break;
            case "Vehiculo":
                condicion = "Placas";
                break;
        }
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs=stmt.executeQuery("SELECT * FROM "+table+" WHERE "+condicion+"='"+name+"'");
            while(rs.next()){
                ID = Integer.parseInt(rs.getString(1));
            }
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
        return ID;
    }
    
    public void executeSQL(String sql){
        try {
            Statement stmt = conexion.createStatement();
            ResultSet rs=stmt.executeQuery(sql);
            rs.close();
            stmt.close();
        } catch (SQLException e) {
            System.out.print("Error: "+e.getMessage());
        }
    }
}