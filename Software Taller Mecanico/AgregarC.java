import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class AgregarC extends Frame implements ActionListener{

Button Aceptar,Cancelar;
TextField Nombre,Id,Telefono,Domicilio;
String N,D,T;
Panel P;

public AgregarC(){
setTitle("Agregar");
resize(700,500);
P = new Panel(null);
Aceptar = new Button("Aceptar");
Cancelar = new Button("Cancelar");
Nombre = new  TextField(50);
Telefono = new TextField(10);
Domicilio = new TextField(30);
Id = new TextField(5);
P.add(Aceptar);
P.add(Cancelar);
P.add(Nombre);
P.add(Telefono);
P.add(Id);
P.add(Domicilio);
      Label l = new Label("AGREGANDO CLIENTE");
      l.setBounds(100,10,200,15);
      l.setFont(new Font("SanSerif", Font.BOLD, 18));
      P.add(l);
P.add(new Label("Id")).setBounds(80,50,15,15);
P.add(new Label("Nombre:")).setBounds(80,100,60,15);
P.add(new Label("Domicilio:")).setBounds(80,150,60,15);
P.add(new Label("Telefono:")).setBounds(80,200,60,15);
Id.setBounds(150,50,80,20);
Id.setEditable(false);
Nombre.setBounds(150,100,250,20);
Domicilio.setBounds(150,150,250,20);
Telefono.setBounds(150,200,250,20);
Aceptar.addActionListener(this);
Aceptar.setBounds(200,280,100,30);

Cancelar.addActionListener(this);
Cancelar.setBounds(330,280,100,30);
add(P);
setVisible(true);
addWindowListener( new WindowAdapter(){
public void windowClosing(WindowEvent x){
setVisible(false);}});

}
public void actionPerformed(ActionEvent x){
if(x.getSource()==Aceptar)
{JOptionPane.showMessageDialog(null,"Registro exitoso");}
else if(x.getSource()==Cancelar){
setVisible(false);
}
}


public static void main(String [] args){
new AgregarC();
}
}