import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/*
*  Jesus Said Llamas Manriquez
*  Modificar servicio
*  VALIDADO
*/
public class ModificarServicio extends Frame implements ActionListener{
   
   TextField costo, nombre;
   Button actualizar, cancelar;
   Choice servicios;
   
   public ModificarServicio(){
      setLayout(null);
      
      setTitle("Taller Mecanico | Modificar servicio");
      
      Label l = new Label("Modificar servicio");
      l.setBounds(80,60,200,20);
      l.setFont(new Font("SanSerif", Font.BOLD, 18));
      
      Label l4 = new Label("Seleccionar servicio: ");
      l4.setBounds(20, 100, 130,25);
      
      servicios = new Choice();
      servicios.add("Cambio de llantas");
      servicios.setBounds(150, 100, 200, 20);
      
      actualizar = new Button("Actualizar");
      actualizar.setBounds(40, 220, 120, 25);
      actualizar.addActionListener(this);
      
      cancelar = new Button("Cancelar");
      cancelar.setBounds(200, 220, 120, 25);
      cancelar.addActionListener(this);
      
      
      Label  l2 = new Label("Nuevo nombre: ");
      l2.setBounds(20, 140, 100, 20);
      
      Label  l3 = new Label("Nuevo costo: ");
      l3.setBounds(20, 180, 100, 20);
      
      nombre = new TextField();
      nombre.setBounds(150, 140, 200, 20);
      
      costo = new TextField();
      costo.setBounds(150,180,200,20);
      
      add(l);
      add(l2);
      add(l3);
      add(l4);
      add(costo);
      add(nombre);
      add(servicios);
      add(actualizar);
      add(cancelar);
   }

   public void actionPerformed(ActionEvent e){
      if(e.getSource() == actualizar){
         int result = JOptionPane.showConfirmDialog(null,"Confirmacion. \nActualizar: "+servicios.getSelectedItem()+"\nNuevo nombre: "+nombre.getText()+"\nCosto anterior: "+Math.random()*334+"\nNuevo costo: "+costo.getText(),null, JOptionPane.YES_NO_OPTION);
         if(result == JOptionPane.YES_OPTION){
            try{
               int txtCosto = Integer.parseInt(costo.getText());
               JOptionPane.showMessageDialog(null, "Actualizado correctamente.");
            }catch(Exception i){
               JOptionPane.showMessageDialog(null, "Ocurrio un error en el costo del servicio. No se agrego nada.");
            }
         }
      }else if(e.getSource() == cancelar){
         this.setVisible(false);
      }
   }

   public static void main(String[] args){
      ModificarServicio app = new ModificarServicio();
      app.setBounds(10,20,370,270);
      app.setVisible(true);
      app.setResizable(false);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }
}