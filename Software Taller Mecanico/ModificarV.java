import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class ModificarV extends Frame implements ActionListener{

Button Aceptar,Cancelar;
TextField Marca,Id,Modelo,Placas,Obser;
String M,Mo,Pla,Ob;
Panel P;

public ModificarV(){
setTitle("Agregar");
P = new Panel(null);
Aceptar = new Button("Aceptar");
Cancelar = new Button("Cancelar");
Marca = new  TextField(50);
Modelo = new TextField(10);
Placas = new TextField(30);
Obser = new TextField(50);
Id = new TextField(5);
P.add(Aceptar);
P.add(Cancelar);
P.add(Marca);
P.add(Modelo);
P.add(Placas);
P.add(Obser);
      Label l = new Label("MODIFICANDO DATOS");
      l.setBounds(100,30,200,15);
      l.setFont(new Font("SanSerif", Font.BOLD, 18));
      P.add(l);
P.add(new Label("Marca:")).setBounds(80,100,60,15);
P.add(new Label("Modelo:")).setBounds(80,150,60,15);
P.add(new Label("Placas:")).setBounds(80,200,60,15);
P.add(new Label("Observaciones:")).setBounds(80,250,100,15);

Marca.setBounds(190,100,250,20);
Modelo.setBounds(190,150,250,20);

Placas.setBounds(190,200,250,20);

Obser.setBounds(190,250,250,20);
Aceptar.addActionListener(this);
Aceptar.setBounds(200,350,100,30);

Cancelar.addActionListener(this);
Cancelar.setBounds(330,350,100,30);
add(P);
setVisible(true);
addWindowListener( new WindowAdapter(){
public void windowClosing(WindowEvent x){
setVisible(false);}});
setBounds(500,100,550,500);
      setVisible(true);
}
public void actionPerformed(ActionEvent x){
if(x.getSource()==Aceptar)
{JOptionPane.showMessageDialog(null,"Registro Exitoso");}
else if(x.getSource()==Cancelar){
setVisible(false);
}
}


public static void main(String [] args){
new ModificarV();
}
}