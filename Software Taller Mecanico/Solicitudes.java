import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
*  Jesus Said Llamas Manriquez
*  VALIDADO
*/

public class Solicitudes extends Frame implements ActionListener, ItemListener{
   
   Choice clientes, servicio, automovil;
   Choice diaE, mesE, a�oE, diaS, mesS, a�oS;
   Button a�adir, cancelar;
   Label total;
   
   public Solicitudes(){
      setLayout(null);
      setTitle("Taller Mecanico | Solicitud de servicio");
      
      Label l = new Label("Solicitud de servicio");
      l.setBounds(80,50,200,20);
      l.setFont(new Font("SanSerif", Font.BOLD, 18));
      
      Label  l2 = new Label("Cliente: ");
      l2.setBounds(20, 90, 50, 20);
      
      Label  l3 = new Label("Automovil: ");
      l3.setBounds(20, 130, 55, 20);
      
      Label  l4 = new Label("Servicio: ");
      l4.setBounds(20, 170, 50, 20);
      
      Label  l5 = new Label("Entrada: ");
      l5.setBounds(20, 210, 50, 20);
      
      Label  l6 = new Label("Salida: ");
      l6.setBounds(20, 250, 50, 20);

      clientes = new Choice();
      clientes.add("Regina");
      clientes.setBounds(80,90,245,20);
      
      automovil = new Choice();
      automovil.add("BWM 2019");
      automovil.setBounds(80,130,245,20);

      servicio = new Choice();
      servicio.add("Cambio de llantas");
      servicio.add("Cambio de motor");
      servicio.addItemListener(this);
      servicio.setBounds(80,170,245,20);
      
      diaE = new Choice();
      for (int i = 1; i < 32; i++) {
         diaE.add(""+i);
      }
      diaE.setBounds(80,210,40,20);
      mesE = new Choice();
      mesE.add("Enero");
      mesE.add("Febrero");
      mesE.add("Marzo");
      mesE.add("Abril");
      mesE.add("Mayo");
      mesE.add("Junio");
      mesE.add("Julio");
      mesE.add("Agosto");
      mesE.add("Septiembre");
      mesE.add("Octubre");
      mesE.add("Noviembre");
      mesE.add("Diciembre");
      mesE.setBounds(130, 210, 100,20);
      a�oE = new Choice();
      for (int i = 2015; i < 2050; i++) {
         a�oE.add(""+i);
      }
      a�oE.setBounds(240, 210, 80, 20);
      

      diaS = new Choice();
      for (int i = 1; i < 32; i++) {
         diaS.add(""+i);
      }
      diaS.setBounds(80,250, 40,20);
      mesS = new Choice();
      mesS.add("Enero");
      mesS.add("Febrero");
      mesS.add("Marzo");
      mesS.add("Abril");
      mesS.add("Mayo");
      mesS.add("Junio");
      mesS.add("Julio");
      mesS.add("Agosto");
      mesS.add("Septiembre");
      mesS.add("Octubre");
      mesS.add("Noviembre");
      mesS.add("Diciembre");
      mesS.setBounds(130, 250, 100,20);
      a�oS = new Choice();
      for (int i = 2015; i < 2050; i++) {
         a�oS.add(""+i);
      }
      a�oS.setBounds(240, 250, 80, 20);

      total = new Label("Total: $ 0.00");
      total.setFont(new Font("SanSerif", Font.BOLD, 14));
      total.setBounds(40, 285, 300, 45);

      a�adir = new Button("A�adir");
      a�adir.setBounds(60, 340, 100, 25);
      a�adir.addActionListener(this);
      
      cancelar = new Button("Cancelar");
      cancelar.setBounds(180,340, 100, 25);
      cancelar.addActionListener(this);
      
      add(l);
      add(l2);
      add(l3);
      add(l4);
      add(l5);
      add(l6);
      add(total);
      add(automovil);
      add(diaE);add(mesE);add(a�oE);
      add(diaS);add(mesS);add(a�oS);
      add(servicio);
      add(clientes);
      add(a�adir);
      add(cancelar);


      setBounds(500,100,350,400);
      setVisible(true);
      addWindowListener( new WindowAdapter(){
         public void windowClosing(WindowEvent x){
            setVisible(false);
         }
      });
   }

   public void itemStateChanged(ItemEvent e){
      //hacer consulta sql para recoger los precios y asignarlos
      total.setText("Total: $ "+Math.random()*2932);
   }  

   public void actionPerformed(ActionEvent e){//ACTIONPERFORMED
      if(e.getSource() == cancelar){
         this.setVisible(false);
      }else if(e.getSource() == a�adir){
         String txtCliente = clientes.getSelectedItem();
         String txtServicio = servicio.getSelectedItem();
         String txtAutomovil = automovil.getSelectedItem();
         String txtFechaS = ""+ diaS.getSelectedItem()+"/"+mesS.getSelectedItem()+"/"+a�oS.getSelectedItem();
         String txtFechaE = ""+ diaE.getSelectedItem()+"/"+mesE.getSelectedItem()+"/"+a�oE.getSelectedItem();
         int result = JOptionPane.showConfirmDialog(null, 
            "Cliente: "+txtCliente+"\n"+"Servicio: "+txtServicio+"\n"+"Automovil: "+txtAutomovil+"\n"+"Fecha de entrada: "+txtFechaE+"\n"+"Fecha de salida: "+txtFechaS,null, JOptionPane.YES_NO_OPTION);
         if(result == JOptionPane.YES_OPTION) {
             boolean isCorecto = validarSolicitud(Integer.parseInt(diaE.getSelectedItem()), Integer.parseInt(diaS.getSelectedItem()), mesE.getSelectedIndex(), mesS.getSelectedIndex(), Integer.parseInt(a�oE.getSelectedItem()), Integer.parseInt(a�oS.getSelectedItem()));
            if(isCorecto){
               JOptionPane.showMessageDialog(null, "Registrada correctamente.");
            }else{
               JOptionPane.showMessageDialog(null, "Implosible registrar. Revisa la fechas (la fecha de salida no puede ser menor que la de entrada)");
            }
         }
      }
   }//ACTIONPERFORMED

   public boolean validarSolicitud(int de, int ds, int me, int ms, int ae, int as){
      boolean val = true;
      if(ms < me | as < ae) val = false;
      if(me == ms && ds < de) val = false; //dia salida menor que dia entrada en mismo mes
      if(me == ms && ds == de && as == ae)  val = false;//no modifico la fecha
      return val;
   }

   public static void main(String[] args){
    new Solicitudes();
    /*  Solicitudes app = new Solicitudes();
      app.setBounds(10,20,310,340);
      app.setVisible(true);
      app.setResizable(false);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });*/
   }
}