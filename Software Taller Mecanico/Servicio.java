import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

/*
*  Jesus Said Llamas Manriquez
*  A�adir servicio
*  VALIDADO
*/
public class Servicio extends Frame implements ActionListener{
   
   TextField costo, nombre;
   Button a�adir, cancelar;
   
   public Servicio(){
      setLayout(null);
      setTitle("Taller Mecanico | A�adir servicio");
      
      a�adir = new Button("A�adir");
      a�adir.setBounds(40, 185, 100, 25);
      a�adir.addActionListener(this);
      
      cancelar = new Button("Cancelar");
      cancelar.setBounds(160,185, 100, 25);
      cancelar.addActionListener(this);
      
      Label l = new Label("A�adir servicio");
      l.setBounds(80,60,200,20);
      l.setFont(new Font("SanSerif", Font.BOLD, 18));
      
      Label  l2 = new Label("Nombre: ");
      l2.setBounds(20, 100, 50, 20);
      
      Label  l3 = new Label("Costo: ");
      l3.setBounds(20, 140, 50, 20);
      
      nombre = new TextField();
      nombre.setBounds(80, 100, 200, 20);
      
      costo = new TextField();
      costo.setBounds(80,140,200,20);
      
      add(l);
      add(l2);
      add(l3);
      add(costo);
      add(nombre);
      add(a�adir);
      add(cancelar);
      setBounds(500,100,300,400);
      setVisible(true);
      addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent x){
            setVisible(false);
         }
      });
   }

   public void actionPerformed(ActionEvent e){
      if(e.getSource() == a�adir){
         int result = JOptionPane.showConfirmDialog(null,"Confirmacion. \nA�adir: "+nombre.getText()+"\nCosto: "+costo.getText(),null, JOptionPane.YES_NO_OPTION);
         if(result == JOptionPane.YES_OPTION){
            try{
               int txtCosto = Integer.parseInt(costo.getText());
               JOptionPane.showMessageDialog(null, "Agregado correctamente.");
            }catch(Exception i){
               JOptionPane.showMessageDialog(null, "Ocurio un error en el costo del servicio. No se agrego nada.");
            }
         }
      }else if(e.getSource() == cancelar){
         this.setVisible(false);
      }
   }

   public static void main(String[] args){
   new Servicio();
      /*Servicio app = new Servicio();
      app.setBounds(10,20,310,230);
      app.setVisible(true);
      app.setResizable(false);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });*/
   }
}