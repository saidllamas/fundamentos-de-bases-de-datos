import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class EliminarV extends Frame implements ActionListener{

Button Aceptar,Cancelar;
TextField Marca,Id,Modelo,Placas,Obser;
String M,Mo,Pla,Ob;
Panel P;

public EliminarV(){
setTitle("Eliminar");
P = new Panel(null);
Aceptar = new Button("Aceptar");
Cancelar = new Button("Cancelar");
Marca = new  TextField(50);
Modelo = new TextField(10);
Placas = new TextField(30);
Id = new TextField(5);
P.add(Aceptar);
P.add(Cancelar);
P.add(Marca);
P.add(Modelo);
P.add(Id);
P.add(Placas);
  Label l = new Label("ELIMINANDO DATOS");
      l.setBounds(100,10,200,15);
      l.setFont(new Font("SanSerif", Font.BOLD, 18));
      P.add(l);
P.add(new Label("Id")).setBounds(80,50,15,15);
P.add(new Label("Marca:")).setBounds(80,100,60,15);
P.add(new Label("Modelo:")).setBounds(80,150,60,15);
P.add(new Label("Placas:")).setBounds(80,200,60,15);
Id.setBounds(190,50,80,20);

Marca.setBounds(190,100,250,20);
Modelo.setBounds(190,150,250,20);

Placas.setBounds(190,200,250,20);

Aceptar.addActionListener(this);
Aceptar.setBounds(180,250,100,30);

Cancelar.addActionListener(this);
Cancelar.setBounds(300,250,100,30);
add(P);
setVisible(true);
addWindowListener( new WindowAdapter(){
public void windowClosing(WindowEvent x){
setVisible(false);}});
setBounds(500,100,550,400);
      setVisible(true);
}
public void actionPerformed(ActionEvent x){
if(x.getSource()==Aceptar)
{JOptionPane.showMessageDialog(null,"Vehiculo Eliminado");}
else if(x.getSource()==Cancelar){
setVisible(false);
}
}


public static void main(String [] args){
new EliminarV();
}
}