import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/*
*  Jesus Said Llamas Manriquez
*  Eliminar solicitudes
*  VALIDADO
*/
public class EliminarSolicitudes extends Frame implements ActionListener{
   
   TextField costo, nombre;
   Button borrar, cancelar;
   Choice solicitudes;
   
   public EliminarSolicitudes(){
      setLayout(null);
      
      setTitle("Taller Mecanico | Eliminar servicio");
      
      Label l = new Label("Eliminar solicitud");
      l.setBounds(80,60,200,20);
      l.setFont(new Font("SanSerif", Font.BOLD, 18));
      
      Label l4 = new Label("Seleccionar solicitud: ");
      l4.setBounds(20, 100, 130,25);
      
      solicitudes = new Choice();
      solicitudes.add("Cambio de llantas - Saul Jimenez - 23/03/2017");
      solicitudes.setBounds(150, 100, 200, 20);
      
      borrar = new Button("Borrar");
      borrar.setBounds(40, 140, 120, 25);
      borrar.addActionListener(this);
      
      cancelar = new Button("Cancelar");
      cancelar.setBounds(200,140, 120, 25);
      cancelar.addActionListener(this);
      
      
           
      add(l);
      add(l4);
      add(solicitudes);
      add(borrar);
      add(cancelar);
   }

   public void actionPerformed(ActionEvent e){
      if (e.getSource() == borrar) {
         int result = JOptionPane.showConfirmDialog(null, 
            "�De verdad quieres eliminar la solicitud " + solicitudes.getSelectedItem() + "?",null, JOptionPane.YES_NO_OPTION);
         if(result == JOptionPane.YES_OPTION) {
            solicitudes.removeAll();
            JOptionPane.showMessageDialog(null, "Eliminado correctamente");
         }
      }else if (e.getSource() == cancelar) {
         this.setVisible(false);
      }
   }

   public static void main(String[] args){
      EliminarSolicitudes app = new EliminarSolicitudes();
      app.setBounds(10,20,370,190);
      app.setVisible(true);
      app.setResizable(false);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }
}