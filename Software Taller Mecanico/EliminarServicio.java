import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
/*
*  Jesus Said Llamas Manriquez
*  Eliminar servicio
*  VALIDADO
*/
public class EliminarServicio extends Frame implements ActionListener{
   
   TextField costo, nombre;
   Button borrar, cancelar;
   Choice servicios;
   
   public EliminarServicio(){
      setLayout(null);
      
      setTitle("Taller Mecanico | Eliminar servicio");
      
      Label l = new Label("Eliminar servicio");
      l.setBounds(80,60,200,20);
      l.setFont(new Font("SanSerif", Font.BOLD, 18));
      
      Label l4 = new Label("Seleccionar servicio: ");
      l4.setBounds(20, 100, 130,25);
      
      servicios = new Choice();
      servicios.add("Cambio de llantas");
      servicios.setBounds(150, 100, 200, 20);
      
      borrar = new Button("Borrar");
      borrar.setBounds(40, 140, 120, 25);
      borrar.addActionListener(this);
      
      cancelar = new Button("Cancelar");
      cancelar.setBounds(200,140, 120, 25);
      cancelar.addActionListener(this);
      
      
           
      add(l);
      add(l4);
      add(servicios);
      add(borrar);
      add(cancelar);
   }

   public void actionPerformed(ActionEvent e){
      if (e.getSource() == borrar) {
         int result = JOptionPane.showConfirmDialog(null, 
            "¿De verdad quieres eliminar el servicio " + servicios.getSelectedItem() + "?",null, JOptionPane.YES_NO_OPTION);
         if(result == JOptionPane.YES_OPTION) {
            servicios.removeAll();
            JOptionPane.showMessageDialog(null, "Eliminado correctamente");
         }
      }else if (e.getSource() == cancelar) {
         this.setVisible(false);
      }
   }

   public static void main(String[] args){
      EliminarServicio app = new EliminarServicio();
      app.setBounds(10,20,370,190);
      app.setVisible(true);
      app.setResizable(false);
      app.addWindowListener(new WindowAdapter(){
         public void windowClosing(WindowEvent e){
            System.exit(0);
         }
      });
   }
}