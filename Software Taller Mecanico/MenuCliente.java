import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MenuCliente extends Frame implements ActionListener {

Button Agregar,Eliminar,Consultar,Modificar,Salir;
Panel P;

public MenuCliente(){

super("Clientes");
resize(500,500);
P = new Panel(null);

Agregar = new Button("Agregar");
Eliminar = new Button("Eliminar");
Consultar = new Button("Consultar");
Modificar = new Button("Modificar");
Salir = new Button("Salir");
P.add(Agregar);
P.add(Eliminar);
P.add(Modificar);
P.add(Consultar);
P.add(Salir);

      Label l = new Label("MENU CLIENTE");
      l.setBounds(150,50,200,15);
      l.setFont(new Font("SanSerif", Font.BOLD, 18));
      P.add(l);
Agregar.addActionListener(this);
Agregar.setBounds(150,200,100,30);
Eliminar.addActionListener(this);
Eliminar.setBounds(280,200,100,30);
Modificar.addActionListener(this);
Modificar.setBounds(150,270,100,30);
Consultar.addActionListener(this);
Consultar.setBounds(280,270,100,30);
Salir.addActionListener(this);
Salir.setBounds(280,330,100,30);
P.setBackground(new Color(207,251,254));
add(P);
setVisible(true);
addWindowListener( new WindowAdapter(){
public void windowClosing(WindowEvent x){
setVisible(false);}});
}

public void actionPerformed(ActionEvent x){
if(x.getSource()==Agregar){
new AgregarC();
}
else if(x.getSource()==Salir){
setVisible(false);
}else if (x.getSource()==Eliminar){
new EliminarC();
}else if(x.getSource()==Consultar){
new ConsultarC();
}else if(x.getSource()==Modificar){
new BuscandoC();
}

}

public static void main(String [] args){
new MenuCliente();
}

}//CLASS