import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class MenuPrincipal extends Frame implements ActionListener {

Button Clientes,Servicios,Vehiculos,Garantia,Solicita,EncabSolicita,Salir;
Panel P;

public MenuPrincipal(){
super("Menu Principal");
resize(700,400);
P = new Panel(null);
Clientes = new Button("Clientes");
Servicios = new Button("Servicios");
Vehiculos = new Button("Vehiculos");
Garantia = new Button("Garantia");
Solicita = new Button("Solicita");
Salir = new Button("Salir");

EncabSolicita = new Button("Encab_Sol");

P.add(Clientes);
P.add(Servicios);
P.add(Vehiculos);
P.add(Garantia);
P.add(Solicita);
P.add(Salir);
P.add(EncabSolicita);
      Label l = new Label("MENU PRINCIPAL");
      l.setBounds(100,10,200,15);
      l.setFont(new Font("SanSerif", Font.BOLD, 18));
      P.add(l);
Clientes.addActionListener(this);
Clientes.setBounds(80,150,100,30);
Servicios.addActionListener(this);
Servicios.setBounds(200,150,100,30);
Vehiculos.addActionListener(this);
Vehiculos.setBounds(320,150,100,30);
Solicita.addActionListener(this);
Solicita.setBounds(440,150,100,30);
Garantia.addActionListener(this);
Garantia.setBounds(560,150,100,30);
P.setBackground(new Color(206,255,208));
Salir.addActionListener(this);
Salir.setBounds(320,200,100,30);


add(P);



setVisible(true);
addWindowListener( new WindowAdapter(){
public void windowClosing(WindowEvent x){
setVisible(false);}});



}

public void actionPerformed(ActionEvent x){
if(x.getSource()==Clientes){
new MenuCliente();}
else if(x.getSource()==Vehiculos){
new MenuVehiculo();}
else if(x.getSource()==Salir){
JOptionPane.showMessageDialog(null,"  Hasta luego n.n  ");
setVisible(false);
}else if (x.getSource()==Solicita){
new Solicitudes();
}else if(x.getSource()==Servicios){
new Servicio();
}

}

public static void main(String [] args){
new MenuPrincipal();
}
}