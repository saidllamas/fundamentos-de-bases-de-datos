import java.sql.*;

public class Conexion{
	
	Connection conexion = realizarConexion("jdbc:sqlserver://localhost:1433;"+"DatabaseName=TallerMecanico;","root","root");
	
	public Conexion(){
		registrarJDBC("com.microsoft.sqlserver.jdbc.SQLServerDriver");
		try {
			Statement stmt = conexion.createStatement();
			//consulta a todas las tablas
			ResultSet rs=stmt.executeQuery("select * from Clientes");
			ResultSet rs2=stmt.executeQuery("select * from Vehiculo");
			ResultSet rs3=stmt.executeQuery("select * from Servicio");
			ResultSet rs4=stmt.executeQuery("select * from Solicita");
			rs.close();
			stmt.close();
			System.out.println("Preparado.");
		} catch (SQLException e) {
			System.out.print("Error: "+e.getMessage());
		}
	}

   static public boolean registrarJDBC(String driver){
	   try{
		   Class.forName(driver);
		   return true;
	   	}catch(ClassNotFoundException ex){
	   		return false;
   		}
   }
      
   static public Connection realizarConexion(String url,String user,String pass){
	   try{
		   Connection con;
		   con = DriverManager.getConnection(url,user,pass);
		   return con;
	   }catch(SQLException e){
		   System.out.print("Error: "+e.getMessage());
		   return null;
	   }
   }
   
   public void cerrarConexion(Connection con){
	   try {
		   con.close();
		   System.out.println("Conexi�n cerrada");
	   } catch (SQLException e) {
		   System.out.print("Error: "+e.getMessage());
	   }
   }
   
   public void executeSQL(String sql){
	   try {
			Statement stmt = conexion.createStatement();
			ResultSet rs=stmt.executeQuery(sql);
			rs.close();
			stmt.close();
		 } catch (SQLException e) {
			  System.out.print("Error: "+e.getMessage());
		 }
   }
   
   public void agregarCliente(int idCliente, String nombre, String domicilio, int telefono){
	   try {
			Statement stmt = conexion.createStatement();
			int i =stmt.executeUpdate("INSERT INTO Clientes VALUES("+idCliente+",'"+nombre+"','"+domicilio+"',"+telefono+")");
			if(i>0) System.out.println("Agregado correctamente.");
			stmt.close();
		} catch (SQLException e) {
			System.out.print("Error: "+e.getMessage());
		}
   }
   
   public void editarCliente(int idCliente, String nombre, String domicilio, int telefono){
	   
   }
   
   public void consultarClientes(){
	   try {
			Statement stmt = conexion.createStatement();
			ResultSet rs=stmt.executeQuery("select * from Clientes");
			while(rs.next()){
				System.out.println(rs.getString(1)+", "+rs.getString(2)+", "+rs.getString(3)+", "+rs.getString(4));
			}
			rs.close();
			stmt.close();
		 } catch (SQLException e) {
			  System.out.print("Error: "+e.getMessage());
		 }
   }
   
   public void consultarClientes(String nombre){
	   try {
			Statement stmt = conexion.createStatement();
			ResultSet rs=stmt.executeQuery("select * from Clientes where Nombre='"+nombre+"'");
			while(rs.next()){
				System.out.println(rs.getString(1)+", "+rs.getString(2)+", "+rs.getString(3)+", "+rs.getString(4));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.print("Error: "+e.getMessage());
		}
   }
   
   public void eliminarCliente(String nombre){
	   try {
			Statement stmt = conexion.createStatement();
			int i =stmt.executeUpdate("DELETE FROM Clientes WHERE Nombre ='"+nombre+"'");
			if(i>0) System.out.println("Eliminado correctamente.");
			stmt.close();
		} catch (SQLException e) {
			System.out.print("Error: "+e.getMessage());
		}
   }
   
   /*		Vehiculos		*/
   
   public void agregarVehiculo(int idVehiculo, int idCliente, String marca, String modelo, String placas, String observaciones){
	   try {
			Statement stmt = conexion.createStatement();
			int i =stmt.executeUpdate("INSERT INTO Vehiculo VALUES("+idVehiculo+","+idCliente+",'"+marca+"','"+modelo+"','"+placas+"','"+observaciones+"')");
			if(i>0) System.out.println("Agregado correctamente.");
			stmt.close();
		} catch (SQLException e) {
			System.out.print("Error: "+e.getMessage());
		}
   }
   
   public void editarVehiculo(int idVehiculo, int idCliente, String marca, String modelo, String placas, String observaciones){
	   
   }
   
   public void consultarVehiculos(){
	   try {
			Statement stmt = conexion.createStatement();
			ResultSet rs=stmt.executeQuery("select * from Vehiculo");
			while(rs.next()){
				System.out.println(rs.getString(1)+", "+rs.getString(2)+", "+rs.getString(3)+", "+rs.getString(4)+", "+rs.getString(5)+", "+rs.getString(6));
			}
			rs.close();
			stmt.close();
		 } catch (SQLException e) {
			  System.out.print("Error: "+e.getMessage());
		 }
   }
   
   public void consultarVehiculos(String nombreCliente){
	   try {
			Statement stmt = conexion.createStatement();
			ResultSet rs=stmt.executeQuery("select * from Vehiculo, Clientes where Vehiculo.idCliente=Clientes.idCliente AND Nombre='"+nombreCliente+"'");
			while(rs.next()){
				System.out.println(rs.getString(1)+", "+rs.getString(2)+", "+rs.getString(3)+", "+rs.getString(4)+", "+rs.getString(5)+", "+rs.getString(6));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.print("Error: "+e.getMessage());
		}
   }
   
   public void eliminarVehiculo(String placas){
	   try {
			Statement stmt = conexion.createStatement();
			int i =stmt.executeUpdate("DELETE FROM Vehiculo WHERE Placas ='"+placas+"'");
			if(i>0) System.out.println("Eliminado correctamente.");
			stmt.close();
		} catch (SQLException e) {
			System.out.print("r: "+e.getMessage());
		}
   }
   
   /*		Solcitudes		*/
   
}
